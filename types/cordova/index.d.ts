interface Navigator {
  app: {
    exitApp: () => any; // Or whatever is the type of the exitApp function
  };
}

interface Window {
  device: Device;
  GameAnalytics: any;
  IronSourceAds: any;
  __crashes__: number;
}

interface CordovaPlugins {
  exit(): any;
  notification: any;
}
