declare global {
  interface Window {
    game: Game;
    device: Device;
  }
}

import 'phaser';
import { NinePatchPlugin } from '@koreez/phaser3-ninepatch';
import 'reflect-metadata';
import { isNullOrUndefined } from 'util';
import { gameConfig, IConfig } from './constants/GameConfig';
import { Game } from './Game';
import { ScalingVariant } from './utils/assetLoader';

const SpineWebGLPlugin: any = require('../scripts/SpineWebGLPlugin.js');

export let scaling: ScalingVariant = ScalingVariant.HD;

function setUpDimension(): void {
  const designWidth: number = +process.env.DESIGN_WIDTH;
  const designHeight: number = +process.env.DESIGN_HEIGHT;
  // if (isSlowDevice()) {
  //   designWidth /= 2;
  //   designHeight /= 2;
  //   SCALING_VARIANT = ScalingVariant.SD;
  // }
  if (!window.cordova && __ENV__ !== 'device') {
    gameConfig.canvasWidth = designWidth;
    gameConfig.canvasHeight = designHeight;
    return;
  }
  const width: number = window.screen.width * window.devicePixelRatio;
  const height: number = window.screen.height * window.devicePixelRatio;
  const designRatio: number = designWidth / designHeight;
  const canvasRatio: number = width / height;
  const ratioMultiplier: number = designRatio / canvasRatio;
  gameConfig.canvasWidth = designWidth * (width < height ? 1 : ratioMultiplier);
  gameConfig.canvasHeight =
    designHeight * (width < height ? ratioMultiplier : 1);
}

function startGame(): void {
  const config: IConfig = {
    type: Phaser.WEBGL,
    width: gameConfig.canvasWidth,
    height: gameConfig.canvasHeight,
    parent: 'game-container',
    scene: [],
    transparent: false,
    plugins: {
      global: [
        { key: 'NinePatchPlugin', plugin: NinePatchPlugin, start: true },
      ],
      scene: [
        {
          key: 'SpineWebGLPlugin',
          // @ts-ignore
          mapping: 'spine',
          // @ts-ignore
          plugin: SpineWebGLPlugin,
        },
      ],
    },
  };
  window.game = new Game(config);

  console.log('Game Started!');

  console.log('width :', config.width, 'height :', config.height);

  console.log(
    'scaleX :',
    config.width / +process.env.DESIGN_WIDTH,
    'scaleY :',
    config.height / +process.env.DESIGN_HEIGHT,
  );
}

function loadWebFont(callback: () => any): void {
  setTimeout(() => {
    callback();
  }, 200);
}

window.onload = () => {
  if (!window.cordova && __ENV__ === 'production') {
    // tslint:disable-next-line:typedef no-require-imports
    const Sentry = require('@sentry/browser');
    Sentry.init({
      dsn: process.env.SENTRY_DSN_JS,
    });
  }
  loadWebFont(() => {
    setUpDimension();
    startGame();
  });
};

document.addEventListener('deviceready', () => {
  if (window.cordova) {
    // tslint:disable-next-line:typedef
    if (window.cordova.platformId === 'android') {
      // @ts-ignore
      window.StatusBar.styleDefault();
      // @ts-ignore
      window.StatusBar.hide();
      if (!isNullOrUndefined(window.AndroidFullScreen)) {
        AndroidFullScreen.isSupported(
          () => {
            AndroidFullScreen.immersiveMode();
          },
          (error: any) => {
            console.error(error);
          },
        );
      }
    }
  }
});
