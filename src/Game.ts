import { Context, MVCSBundle } from '@robotlegsjs/core';
import { ContextSceneManager, PhaserBundle } from '@robotlegsjs/phaser';
import { SignalCommandMapExtension } from '@robotlegsjs/signalcommandmap';
import firebase from 'firebase/app';
import { MediatorConfig } from './config/MediatorConfig';
import { RobotlegsConfig } from './config/RobotlegsConfig';
import { IConfig } from './constants/GameConfig';
import { GAME } from './constants/injectionKeys';
import { SceneKey } from './constants/SceneKey';
import { SceneBoot, SceneMain, ScenePreload } from './scenes';
import { SignalGameStartup } from './signals';

export class Game extends Phaser.Game {
  public static PAUSE: string = 'pause';
  public static RESUME: string = 'resume';
  private robotlegsContext: Context;

  constructor(config: IConfig) {
    super(config);
    this.robotlegsContext = new Context();
    this.robotlegsContext
      .install(MVCSBundle, PhaserBundle, SignalCommandMapExtension)
      .configure(new ContextSceneManager(this.scene))
      .configure(MediatorConfig)
      .configure(RobotlegsConfig)
      .initialize(this.gameInitCallCallBack);
    this.scene.add(SceneKey.BOOT, new SceneBoot(SceneKey.BOOT));
    this.scene.add(SceneKey.PRELOAD, new ScenePreload(SceneKey.PRELOAD));
    this.scene.add(SceneKey.PRELOAD, new SceneMain(SceneKey.MAIN));

    this.robotlegsContext.injector.bind(GAME).toConstantValue(this);

    this.resize();
    this.scene.start(SceneKey.BOOT);
    // this.init();
  }

  public resize(): void {
    const { width, height } = this.config as any;

    const scale: { x: number; y: number } = {
      x: (window.innerWidth || width) / width,
      y: (window.innerHeight || height) / height,
    };
    if (!window.cordova) {
      const browserScale: number = Math.min(
        window.innerHeight / height,
        window.innerWidth / width,
      );
      scale.x = scale.y = browserScale;
    }
    this.canvas.style.position = 'absolute';
    this.canvas.style.width = width * scale.x + 'px';
    this.canvas.style.height = height * scale.y + 'px';
    this.canvas.style.left = (window.innerWidth - width * scale.x) * 0.5 + 'px';
    this.canvas.style.top =
      (window.innerHeight - height * scale.y) * 0.5 + 'px';
  }

  private async init(): Promise<void> {
    await firebase.initializeApp({
      apiKey: 'key',
      authDomain: 'gameName.firebaseapp.com',
      projectId: 'gameId',
    });
    const firestore: firebase.firestore.Firestore = firebase.firestore();
    firestore.settings({ timestampsInSnapshots: true });
  }

  private gameInitCallCallBack = () => {
    const gameStartupSignal: SignalGameStartup = this.robotlegsContext.injector.get(
      SignalGameStartup,
    );
    gameStartupSignal.dispatch();
  };
}
