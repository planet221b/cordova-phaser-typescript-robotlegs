import { inject, injectable } from '@robotlegsjs/core';
import store from 'store';
import { isNullOrUndefined } from 'util';
import { GAME_STORAGE_KEY } from '../constants/StoreKeys';
import { ModelGame } from '../models/ModelGame';
import { ModelUser } from '../models/ModelUser';

@injectable()
export class ManagerSave {
  @inject(ModelGame)
  private modelGame: ModelGame;

  @inject(ModelUser)
  private modelUser: ModelUser;

  private gameStore: StoreJsAPI;

  private throttleGameSave: () => void;

  private throttleUserSave: () => void;

  public start(): void {
    this.gameStore = store.namespace(GAME_STORAGE_KEY);

    if (isNullOrUndefined(window.cordova)) {
      this.addBeforeUnloadEvent();
    } else {
      this.addResignEvent();
    }
  }

  private addBeforeUnloadEvent = () => {
    window.addEventListener('beforeunload', () => {
      this.userSave();
      this.gameSave();
    });
  };

  private addResignEvent = () => {
    document.addEventListener(
      'resign',
      () => {
        this.userSave();
        this.gameSave();
      },
      false,
    );
  };

  private userSave = () => {
    console.info('userSave :');
    this.gameStore.set(ModelUser.STORAGE_KEY, this.modelUser.serialize());
  };

  private gameSave = () => {
    console.info('gameSave :');
    this.gameStore.set(ModelGame.STORAGE_KEY, this.modelGame.serialize());
  };
}
