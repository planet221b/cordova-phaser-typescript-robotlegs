import { injectable } from '@robotlegsjs/core';
import { SignalBase } from './SignalBase';

@injectable()
export class SignalGameStartup extends SignalBase {
  constructor() {
    super();
  }
}
