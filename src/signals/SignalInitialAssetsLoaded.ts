import { injectable } from '@robotlegsjs/core';
import { SignalBase } from './SignalBase';

@injectable()
export class SignalInitialAssetsLoaded extends SignalBase {}
