import { injectable } from '@robotlegsjs/core';
import { SignalBase } from './SignalBase';

@injectable()
export class SignalGamePaused extends SignalBase {}

@injectable()
export class SignalGameResumed extends SignalBase {}
