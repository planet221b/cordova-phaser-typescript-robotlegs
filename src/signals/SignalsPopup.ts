import { injectable } from '@robotlegsjs/core';
import { SignalBase } from './SignalBase';

@injectable()
export class SignalPopupHide extends SignalBase {
  constructor() {
    super(Phaser.GameObjects.GameObject);
  }
}
@injectable()
export class SignalPopupForceClose extends SignalBase {
  constructor() {
    super(Phaser.GameObjects.GameObject);
  }
}

@injectable()
export class SignalPopupExampleShow extends SignalBase {}
