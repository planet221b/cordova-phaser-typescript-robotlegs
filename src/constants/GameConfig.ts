export const gameConfig: ISizeConfig = {
  designWidth: 1080,
  designHeight: 1920,
  canvasWidth: 1080,
  canvasHeight: 1920,
};

export interface IConfig {
  type: number;
  width: number;
  height: number;
  parent: string;
  scene: any[];
  transparent: boolean;
  dom?: any;
  plugins?: any;
}

export interface ISizeConfig {
  designWidth: number;
  designHeight: number;
  canvasWidth: number;
  canvasHeight: number;
}
