import { injectable } from '@robotlegsjs/core';
import { ViewMediator } from '@robotlegsjs/phaser';
import { IReactionOptions, IReactionPublic } from 'mobx';
import BaseMediatorsUtil from '../scenes/BaseMediatorsUtil';
import { BaseView } from './BaseView';

@injectable()
export abstract class AbstractMediatorView<
  T extends BaseView
> extends ViewMediator<T> {
  protected baseMediatorsUtil: BaseMediatorsUtil;

  public initialize(): void {
    this.baseMediatorsUtil = new BaseMediatorsUtil(this);
  }

  public preDestroy(): void {
    this.removeSignalListeners();
    this.removeViewListeners();
  }

  public destroy(): void {
    this.preDestroy();
    this.baseMediatorsUtil.destroy();
    this.baseMediatorsUtil = null;
  }

  protected addReaction<D>(
    expression: (r: IReactionPublic) => D,
    effect: (arg: D, r: IReactionPublic) => void,
    opts?: IReactionOptions,
  ): this {
    this.baseMediatorsUtil.addReaction(expression, effect, opts);
    return this;
  }

  protected removeReaction<D>(
    effect: (arg: D, r: IReactionPublic) => void,
  ): this {
    this.baseMediatorsUtil.removeReaction(effect);
    return this;
  }

  protected addSignalListeners(): void {
    //
  }
  protected removeSignalListeners(): void {
    //
  }

  protected addViewListeners(): void {
    //
  }
  protected removeViewListeners(): void {
    //
  }
}
