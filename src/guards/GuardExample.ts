import { IGuard, inject, injectable } from '@robotlegsjs/core';
import { ModelGame } from '../models/ModelGame';

@injectable()
export class GuardDoesWorkerExist implements IGuard {
  @inject(ModelGame)
  private modelGame: ModelGame;

  @inject(String)
  private value: string;

  public approve(): boolean {
    return !!this.modelGame && !!this.value;
  }
}
