import { isNullOrUndefined } from 'util';
import { gameConfig } from '../constants/GameConfig';
export * from './assetLoader';

const DEVICES_WITH_NOTCH: string[] = [
  'iPhone10,3',
  'iPhone10,6',
  'iPhone11,8',
  'iPhone11,2',
  'iPhone11,4',
  'iPhone11,6',
];

const SLOW_DEVICES: string[] = [
  'iPhone4,1',
  'iPhone5,1',
  'iPhone5,2',
  'iPhone5,3',
  'iPhone5,4',
  'iPhone6,1',
  'iPhone6,2',
  'iPhone7,2',
  'iPhone7,1',
  'iPod5,1',
  'iPod7,1',
  'iPad2,1',
  'iPad2,2',
  'iPad2,3',
  'iPad2,4',
  'iPad3,1',
  'iPad3,2',
  'iPad3,3',
  'iPad3,4',
  'iPad3,5',
  'iPad3,6',
  'iPad4,1',
  'iPad4,2',
  'iPad4,3',
  'iPad5,3',
  'iPad5,4',
  'iPad2,5',
  'iPad2,6',
  'iPad2,7',
  'iPad4,4',
  'iPad4,5',
  'iPad4,6',
  'iPad4,7',
  'iPad4,8',
  'iPad4,9',
];

export const getCircularReplacer: () => any = () => {
  const seen: WeakSet<any> = new WeakSet();
  return (key: string, value: any) => {
    if (typeof value === 'object' && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

export const pad: (d: number) => string = (d: number): string => {
  return d < 10 ? '0' + d.toString() : d.toString();
};

export const getScene: (sceneKey: string) => Phaser.Scene = (
  sceneKey: string,
): Phaser.Scene => window.game.scene.getScene(sceneKey);

export function addByFormattingFixedPointNotation(
  num1: number,
  num2: number,
  digit: number = 2,
): number {
  return +(num1 + num2).toFixed(digit);
}

export function subtractByFormattingFixedPointNotation(
  num1: number,
  num2: number,
  digit: number = 2,
): number {
  return +(num1 - num2).toFixed(digit);
}

export function formatTime(time: number): string {
  return `${Math.floor(time / 60)}m ${time % 60}s`;
}

export function startViewMediator(
  scene: Phaser.Scene,
  view: Phaser.GameObjects.GameObject,
  container: Phaser.GameObjects.Container,
): void {
  scene.add.existing(view);
  container.add(view);
}

export function formatValue(
  value: number,
  fixTo: number = 2,
  roundSmallValues: boolean = false,
): string {
  value = +value.toFixed(2);
  let exponent: number = 0;
  while (1000 ** exponent <= value) {
    exponent++;
  }
  exponent -= 1;
  if (exponent < 0) {
    return value.toString();
  }
  let numberPart: number = +(value / 1000 ** exponent);
  numberPart =
    fixTo === 0
      ? Math.floor(+numberPart.toFixed(3))
      : +numberPart.toFixed(fixTo);
  const postfix: string = generateStringPart(exponent);
  const resultNumber: number =
    fixTo >= 1 || postfix.length > 0
      ? +numberPart.toFixed(numberPart < 100 ? 1 : 0)
      : +numberPart.toFixed(roundSmallValues ? 0 : fixTo);
  return resultNumber + postfix;
}

const ALPHABET: string = 'abcdefghijklmnopqrstuvwxyz';

function generateStringPart(exponent: number): string {
  let result: string = '';
  switch (exponent) {
    case 0:
      break;
    case 1:
      result = 'K';
      break;
    case 2:
      result = 'M';
      break;
    case 3:
      result = 'B';
      break;
    case 4:
      result = 'T';
      break;
    default:
      const firstElementIndex: number = Math.floor(
        (exponent - 5) / ALPHABET.length,
      );
      const secondElementIndex: number = (exponent - 5) % ALPHABET.length;
      result += ALPHABET[firstElementIndex];
      result += ALPHABET[secondElementIndex];
      break;
  }
  return result;
}

export function hasNotch(): boolean {
  if (isNullOrUndefined(window.device)) {
    return false;
  }
  return DEVICES_WITH_NOTCH.indexOf(window.device.model) !== -1;
}

export function isFat(): boolean {
  return (
    gameConfig.canvasWidth / +process.env.DESIGN_WIDTH >
    gameConfig.canvasHeight / +process.env.DESIGN_HEIGHT
  );
}

export function isSlowDevice(): boolean {
  if (isNullOrUndefined(window.device)) {
    return false;
  }
  return SLOW_DEVICES.indexOf(window.device.model) !== -1;
}

export function delayRunnable(
  scene: Phaser.Scene,
  delay: number,
  runnable: any,
  context: any,
  ...args: any[]
): Phaser.Time.TimerEvent {
  return addRunnable(scene, delay, runnable, context, false, ...args);
}

export function loopRunnable(
  scene: Phaser.Scene,
  delay: number,
  runnable: any,
  context: any,
  ...args: any[]
): Phaser.Time.TimerEvent {
  return addRunnable(scene, delay, runnable, context, true, ...args);
}

function addRunnable(
  scene: Phaser.Scene,
  delay: number,
  runnable: (...args: any[]) => any,
  context: any,
  loop: boolean,
  ...args: any[]
): Phaser.Time.TimerEvent {
  return scene.time.addEvent({
    delay,
    callback: runnable,
    callbackScope: context,
    loop,
    args,
  });
}

export function removeRunnable(runnable: Phaser.Time.TimerEvent): void {
  if (!runnable) {
    return;
  }
  runnable.destroy();
}

export function postRunnable(
  scene: Phaser.Scene,
  runnable: any,
  context?: any,
  ...args: any[]
): Phaser.Time.TimerEvent {
  return delayRunnable(
    scene,
    scene.game.loop.delta,
    runnable,
    context,
    ...args,
  );
}

export function stringColorToNumber(color: string): number {
  color.replace('#', '0x');
  return +color;
}

export function wrapLines(
  text: string,
  charWidth: number,
  maxWidth: number,
): string {
  const oneLineMaxSymbolsCount: number = Math.floor(maxWidth / charWidth);
  let lastWord: string = '';
  let lastWordEndIndex: number = 0;
  const words: string[] = text.split(' ');
  let result: string = '';
  for (
    let i: number = 0;
    i <= Math.floor(text.length / oneLineMaxSymbolsCount);
    i++
  ) {
    const remainingWords: string[] = words.filter((word: string) => {
      return text.indexOf(word) >= lastWordEndIndex;
    });
    let newLine: string = '';
    for (const word of remainingWords) {
      const newLength: number = newLine.length + word.length;
      if (newLength <= oneLineMaxSymbolsCount) {
        newLine += `${word} `;
        lastWord = word;
        lastWordEndIndex = text.indexOf(lastWord) + lastWord.length - 1;
      } else {
        break;
      }
    }
    result += `\n${newLine}`;
  }
  return result;
}
