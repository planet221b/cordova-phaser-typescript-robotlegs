import { IConfig, IContext, inject, injectable } from '@robotlegsjs/core';
import { ISignalCommandMap } from '@robotlegsjs/signalcommandmap';
import { CommandGameStartup } from '../commands/game/CommandGameStartup';
import { ManagerSave } from '../managers/ManagerSave';
import { SignalGameStartup } from '../signals';

@injectable()
export class RobotlegsConfig implements IConfig {
  @inject(IContext)
  public context: IContext;

  @inject(ISignalCommandMap)
  public signalCommandMap: ISignalCommandMap;

  public configure(): void {
    this.mapModels();
    this.mapManagers();
    this.mapHelpers();
    this.mapSignalCommands();
    this.mapSignals();
    this.bindDynamicValues();
  }

  private bindDynamicValues(): void {}

  private injectToContext(arr: any): void {
    arr.forEach((item: any) => {
      this.context.injector
        .bind(item)
        .toSelf()
        .inSingletonScope();
    });
  }

  private mapSignals(): void {
    // this.injectToContext([SignalPopupHide]);
  }

  private mapManagers(): void {
    this.injectToContext([ManagerSave]);
  }

  private mapHelpers(): void {
    this.injectToContext([]);
  }

  private mapSignalCommands(): void {
    this.signalCommandMap.map(SignalGameStartup).toCommand(CommandGameStartup);
    // this.signalCommandMap.map(Signal).toCommand(Command). .withGuards(Guard);
  }

  private mapModels(): void {
    // this.injectToContext([ModelGame]);
  }
}
