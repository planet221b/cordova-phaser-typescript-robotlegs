import { IConfig, inject, injectable } from '@robotlegsjs/core';
import { ISceneMediatorMap, IViewMediatorMap } from '@robotlegsjs/phaser';
import { SceneBoot, SceneMain, ScenePreload } from '../scenes';
import { MediatorBoot } from '../scenes/MediatorBoot';
import { MediatorMain } from '../scenes/MediatorMain';
import { MediatorPopup } from '../scenes/MediatorPopup';
import { MediatorPreload } from '../scenes/MediatorPreload';
import { ScenePopup } from '../scenes/ScenePopup';

@injectable()
export class MediatorConfig implements IConfig {
  @inject(ISceneMediatorMap)
  public sceneMediatorMap: ISceneMediatorMap;

  @inject(IViewMediatorMap)
  public viewMediatorMap: IViewMediatorMap;

  public configure(): void {
    this.mapSceneMediators();
    this.mapViewMediators();
  }

  private mapSceneMediators(): void {
    this.sceneMediatorMap.map(SceneBoot).toMediator(MediatorBoot);
    this.sceneMediatorMap.map(ScenePreload).toMediator(MediatorPreload);
    this.sceneMediatorMap.map(SceneMain).toMediator(MediatorMain);
    this.sceneMediatorMap.map(ScenePopup).toMediator(MediatorPopup);
  }

  private mapViewMediators(): void {
    // this.viewMediatorMap
    //   .map(View)
    //   .toMediator(Mediator);
  }
}
