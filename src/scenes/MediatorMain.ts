import { inject, injectable } from '@robotlegsjs/core';
import { SceneMain } from '.';
import { ModelGame } from '../models/ModelGame';
import { SignalGameStartup } from '../signals';
import { AbstractMediatorScene } from './AbstractMediatorScene';

@injectable()
export class MediatorMain extends AbstractMediatorScene<SceneMain> {
  @inject(ModelGame)
  public modelGame: ModelGame;
  @inject(SignalGameStartup)
  public signalGameStartup: SignalGameStartup;

  public initialize(): void {
    super.initialize();
    this.signalGameStartup.dispatch();
  }

  // override
  protected sceneCreated(): void {
    this.addReaction(
      () => ({
        width: this.modelGame.width,
        height: this.modelGame.height,
        rotationDirection: this.modelGame.rotationDirection,
      }),
      this.update,
      { fireImmediately: true },
    );
  }

  protected update(data: {
    width: number;
    height: number;
    rotationDirection: number;
  }): void {
    this.scene.redraw(data.width, data.height, data.rotationDirection);
  }
}
