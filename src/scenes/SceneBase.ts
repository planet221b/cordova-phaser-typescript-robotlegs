import {
  TIsoGameObjectCreator,
  TIsoGameObjectFactory,
} from '@koreez/phaser3-isometric-plugin';
import {
  INinePatchCreator,
  INinePatchFactory,
} from '@koreez/phaser3-ninepatch';
export abstract class SceneBase extends Phaser.Scene {
  public static PLAY_SFX: string = 'playSound';
  public static SHUTDOWN: string = 'shutdown';

  private static readonly consoleArgs: string[] = [
    ``,
    `background: ${'#295A34'}`,
    `background: ${'#2FAA4A'}`,
    `color: ${'#102415'}; background: ${'#27D04C'};`,
    `background: ${'#2FAA4A'}`,
    `background: ${'#295A34'}`,
  ];

  public add: INinePatchFactory & TIsoGameObjectFactory;
  public make: INinePatchCreator & TIsoGameObjectCreator;

  public onCreationCompleteCb: () => void;

  public init(data?: any): void {
    SceneBase.consoleArgs[0] = `%c %c %c ${this.sys.settings.key}: init %c %c `;
    console.log.apply(console, SceneBase.consoleArgs);
  }

  public create(): void {
    SceneBase.consoleArgs[0] = `%c %c %c ${
      this.sys.settings.key
    }: create %c %c `;
    console.log.apply(console, SceneBase.consoleArgs);
    this.sound.volume = 0.5;
    this.handleCreationComplete();
  }

  public shutdown(): void {
    SceneBase.consoleArgs[0] = `%c %c %c ${
      this.sys.settings.key
    }: shutdown %c %c `;
    console.log.apply(console, SceneBase.consoleArgs);
  }

  public playSFX(soundName: string): void {
    this.sound.play(soundName);
  }

  private handleCreationComplete(): void {
    if (this.onCreationCompleteCb) {
      this.onCreationCompleteCb();
    } else {
      console.warn(
        `${this.scene.key} scenes onCreationCompleteCb is not initialized`,
      );
    }
  }
}
