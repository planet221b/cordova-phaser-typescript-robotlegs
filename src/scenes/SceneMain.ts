import { NinePatch } from '@koreez/phaser3-ninepatch';
import { Atlases, Images } from '../assets';
import { gameConfig } from '../constants/GameConfig';
import { SceneBase } from './SceneBase';

export class SceneMain extends SceneBase {
  private ninePatch: NinePatch;
  private direction: number = 1;

  public create(): void {
    const bg: Phaser.GameObjects.Image = this.add.image(
      gameConfig.canvasWidth * 0.5,
      gameConfig.canvasHeight * 0.5,
      Atlases.Main.Atlas.Name,
      Atlases.Main.Atlas.Frames.Bg,
    );
    bg.setScale(
      gameConfig.canvasWidth / bg.width,
      gameConfig.canvasHeight / bg.height,
    );

    this.ninePatch = this.add.ninePatch(
      gameConfig.canvasWidth * 0.5,
      gameConfig.canvasHeight * 0.5,
      300,
      300,
      Images.SquareGreen.Name,
      null,
      {
        bottom: 14, // Amount of pixels for bottom
        left: 6, // Amount of pixels for left
        right: 6, // Amount of pixels for right
        top: 10, // Amount of pixels for top
      },
    );
    super.create();
  }

  public update(): void {
    this.ninePatch.angle += this.direction;
  }

  public redraw(width: number, height: number, direction: number): void {
    this.direction = direction;
    this.ninePatch.resize(width, height);
  }
}
