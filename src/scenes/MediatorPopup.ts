import { inject, injectable } from '@robotlegsjs/core';
import { SceneKey } from '../constants/SceneKey';
import { ModelGame } from '../models/ModelGame';
import { ModelUser } from '../models/ModelUser';
import PopupStandard from '../popups/standard/PopupStandard';
import { postRunnable } from '../utils';
import { AbstractMediatorScene } from './AbstractMediatorScene';
import { ScenePopup } from './ScenePopup';

@injectable()
export class MediatorPopup extends AbstractMediatorScene<ScenePopup> {
  @inject(ModelUser)
  private modelUser: ModelUser;

  @inject(ModelGame)
  private modelGame: ModelGame;

  // @inject(SignalPopupUpgradeShow)
  // private signalPopupUpgradeShow: SignalPopupUpgradeShow;

  // @inject(SignalPopupHide)
  // private signalPopupHide: SignalPopupHide;

  public initialize(): void {
    super.initialize();
    window.game.scene.sendToBack(SceneKey.POPUP);
  }

  protected addSignalListeners(): void {
    // this.signalPopupUpgradeShow.add(this.signalPopupUpgradeShowTriggered);
    // this.signalPopupHide.add(this.signalPopupHideCompleteTriggered);
  }

  // protected signalPopupUpgradeShowTriggered = (workerUuId: string): void => {
  //   this.addPopup(new PopupUpgradeWorker(workerUuId));
  // };

  // protected signalPopupHideCompleteTriggered = (popup: PopupStandard): void => {
  //   this.scene.removePopup(popup);
  //   window.game.scene.sendToBack(SceneKey.POPUP);
  // };

  protected addPopup(popup: PopupStandard): void {
    postRunnable(this.scene, () => {
      window.game.scene.bringToTop(SceneKey.POPUP);
      // window.game.scene.bringToTop(SceneKey.UI);
      this.scene.addPopup(popup);
    });
  }
}
