import { Atlases, Images } from '../assets';
import { SceneKey } from '../constants/SceneKey';
import { loadAtlases, loadImages } from '../utils';
import { SceneBase } from './SceneBase';

export class ScenePreload extends SceneBase {
  public preload(): void {
    loadAtlases(this, Atlases.Main.Atlas);
    loadImages(this, Images);
  }

  public create(): void {
    super.create();
    this.scene.start(SceneKey.MAIN);
    this.scene.remove(this);
  }
}
