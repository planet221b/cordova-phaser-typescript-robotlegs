import { SceneBase } from '.';
import { gameConfig } from '../constants/GameConfig';
import PopupStandard from '../popups/standard/PopupStandard';

export class ScenePopup extends SceneBase {
  public create(): void {
    super.create();
    this.input.dragDistanceThreshold = 20;
    this.input.dragTimeThreshold = 80;
    const zone: Phaser.GameObjects.Zone = this.add.zone(
      gameConfig.canvasWidth / 2,
      gameConfig.canvasHeight / 2,
      gameConfig.canvasWidth,
      gameConfig.canvasHeight,
    );
    zone.setInteractive();
  }

  public addPopup(popup: PopupStandard): void {
    this.add.existing(popup);
  }

  public removePopup(popup: PopupStandard): void {
    popup.destroy();
  }
}
