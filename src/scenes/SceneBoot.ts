import { SceneKey } from '../constants/SceneKey';
import { SceneBase } from './SceneBase';

export class SceneBoot extends SceneBase {
  public create(): void {
    super.create();
    this.scene.start(SceneKey.PRELOAD);
    this.scene.remove(this);
  }
}
