import { injectable } from '@robotlegsjs/core';
import { SceneBoot } from '.';
import { AbstractMediatorScene } from './AbstractMediatorScene';

@injectable()
export class MediatorBoot extends AbstractMediatorScene<SceneBoot> {}
