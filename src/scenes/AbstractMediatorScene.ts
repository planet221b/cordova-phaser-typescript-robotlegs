import { injectable } from '@robotlegsjs/core';
import { SceneMediator } from '@robotlegsjs/phaser';
import { IReactionOptions, IReactionPublic } from 'mobx';
import { SceneBase } from '../scenes/SceneBase';
import BaseMediatorsUtil from './BaseMediatorsUtil';

@injectable()
export abstract class AbstractMediatorScene<
  T extends SceneBase
> extends SceneMediator<T> {
  protected baseMediatorsUtil: BaseMediatorsUtil;

  public initialize(): void {
    this.baseMediatorsUtil = new BaseMediatorsUtil(this);
    this.scene.onCreationCompleteCb = this.sceneCreated.bind(this);
  }

  public preDestroy(): void {
    this.removeSignalListeners();
    this.removeViewListeners();
  }

  public destroy(): void {
    this.baseMediatorsUtil.destroy();
  }

  protected addViewListeners(): void {
    this.removeSignalListeners();
    this.scene.events.on(SceneBase.PLAY_SFX, this.onPlaySound);
  }

  protected removeViewListeners(): void {
    this.scene.events.off(SceneBase.PLAY_SFX, this.onPlaySound, null, false);
  }

  protected addReaction<D>(
    expression: (r: IReactionPublic) => D,
    effect: (arg: D, r: IReactionPublic) => void,
    opts?: IReactionOptions,
  ): this {
    this.baseMediatorsUtil.addReaction(expression, effect, opts);
    return this;
  }

  protected removeReaction<D>(
    effect: (arg: D, r: IReactionPublic) => void,
  ): this {
    this.baseMediatorsUtil.removeReaction(effect);
    return this;
  }

  protected addSignalListeners(): void {
    //
  }

  protected removeSignalListeners(): void {
    //
  }

  protected sceneCreated(): void {
    this.addViewListeners();
    this.addSignalListeners();
  }

  private onPlaySound = (soundName: string, ...args: any[]): void => {
    this.scene.playSFX(soundName);
  };
}
