import { injectable } from '@robotlegsjs/core';
import { ScenePreload } from '.';
import { AbstractMediatorScene } from './AbstractMediatorScene';

@injectable()
export class MediatorPreload extends AbstractMediatorScene<ScenePreload> {}
