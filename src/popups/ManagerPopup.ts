import PopupStandard from './standard/PopupStandard';

export default class ManagerPopup<T extends PopupStandard> {
  private static _instance: ManagerPopup<PopupStandard>;
  private queue: any[] = [];

  public static get instance(): ManagerPopup<PopupStandard> {
    return this._instance || (this._instance = new this());
  }

  public show(popup: T, x: number, y: number, ...args: any[]): void {
    if (this.isPopupAlreadyInQueue(popup)) {
      return;
    }
    this.queue.push({
      _popup: popup,
      _x: x,
      _y: y,
      _args: args,
    });
    if (this.queue.length === 1) {
      this.internalShow();
    }
  }

  public async hide(popup: T, actionId?: number): Promise<void> {
    await popup.hide(actionId);
  }

  public forceHide(popup: T): void {
    popup.forceClosePopup();
    this.popupHideComplete();
  }

  public popupHideComplete(): void {
    this.queue.shift();
    if (this.hasQueue) {
      this.internalShow();
    }
  }

  private internalShow(): void {
    const { _popup, _x, _y, _args } = this.queue[0];
    _popup.show(_x, _y, ..._args);
  }

  private isPopupAlreadyInQueue(popup: any): boolean {
    for (const data of this.queue) {
      if (data._popup.constructor.name === popup.constructor.name) {
        return true;
      }
    }
    return false;
  }

  get hasQueue(): boolean {
    return !!this.queue.length;
  }
}
