import { inject } from '@robotlegsjs/core';
import { gameConfig } from '../../constants/GameConfig';
import { Game } from '../../Game';
import {
  SignalPopupForceClose,
  SignalPopupHide,
} from '../../signals/SignalsPopup';
import { AbstractMediatorView } from '../../views/AbstractMediatorView';
import ManagerPopup from '../ManagerPopup';
import PopupStandard, { PopupEventKeys } from './PopupStandard';

export default abstract class MediatorPopupStandard<
  T extends PopupStandard
> extends AbstractMediatorView<T> {
  @inject(SignalPopupHide)
  private signalPopupHide: SignalPopupHide;

  @inject(SignalPopupForceClose)
  private signalPopupForceClose: SignalPopupForceClose;

  private managerPopup: ManagerPopup<T>;

  // override
  public initialize(): void {
    this.managerPopup = ManagerPopup.instance;
    super.initialize();

    this.showView(gameConfig.canvasWidth / 2, gameConfig.canvasHeight / 2);
    this.addViewListeners();
    this.addSignalListeners();
    this.addReactions();
  }

  protected showView(x: number, y: number, ...args: any[]): void {
    this.managerPopup.show(this.view, x, y, ...args);
  }

  protected async hideView(actionId?: number): Promise<void> {
    await this.managerPopup.hide(this.view, actionId);
  }

  protected forceHide = () => {
    this.managerPopup.forceHide(this.view);
    this.signalPopupForceClose.dispatch(this.view);
  };

  protected addReactions(): void {
    // ...
  }

  protected addSignalListeners(): void {
    // ..
  }

  protected addViewListeners(): void {
    super.addViewListeners();
    this.on(this.view, PopupEventKeys.SHOW_START, this.onViewShowStart);
    this.on(this.view, PopupEventKeys.SHOW_COMPLETE, this.onViewShowComplete);
    this.on(this.view, PopupEventKeys.FORCE_CLOSE, this.onViewForceClose);
    this.on(this.view, PopupEventKeys.HIDE_START, this.onViewHideStart);
    this.on(
      this.view,
      PopupEventKeys.HIDE_COMPLETE,
      this.onViewHideComplete,
      this,
    );
    this.on(this.view, PopupEventKeys.CLOSE, this.onViewClose);
    this.view.scene.game.events.on(Game.PAUSE, this.forceHide);
  }

  protected removeViewListeners(): void {
    super.removeViewListeners();
    this.off(this.view, PopupEventKeys.SHOW_START, this.onViewShowStart);
    this.off(this.view, PopupEventKeys.SHOW_COMPLETE, this.onViewShowComplete);
    this.off(this.view, PopupEventKeys.FORCE_CLOSE, this.onViewForceClose);
    this.off(this.view, PopupEventKeys.HIDE_START, this.onViewHideStart);
    this.off(
      this.view,
      PopupEventKeys.HIDE_COMPLETE,
      this.onViewHideComplete,
      this,
    );
    this.off(this.view, PopupEventKeys.CLOSE, this.onViewClose);
    this.view.scene.game.events.off(Game.PAUSE, this.forceHide, null, false);
  }

  protected onViewShowStart = (): void => {
    // ..
  };

  protected onViewShowComplete = (): void => {
    // ..
  };

  protected onViewHideStart = (): void => {
    // ..
  };

  protected onViewForceClose(): void {
    //
  }

  protected onViewHideComplete(actionId?: number): void {
    this.signalPopupHide.dispatch(this.view);
    this.managerPopup.popupHideComplete();
  }

  protected onViewClose = (actionId?: number): void => {
    this.hideView(actionId);
  };
}
