import { NinePatch } from '@koreez/phaser3-ninepatch';
import { gameConfig } from '../../constants/GameConfig';
import { SceneKey } from '../../constants/SceneKey';
import { SceneBase } from '../../scenes';
import { getScene } from '../../utils';
import { BaseView } from '../../views/BaseView';

export enum PopupEventKeys {
  SHOW_START = 'SHOW_START',
  SHOW_COMPLETE = 'SHOW_COMPLETE',
  FORCE_CLOSE = 'FORCE_CLOSE',
  HIDE_START = 'HIDE_START',
  HIDE_COMPLETE = 'HIDE_COMPLETE',
  CLOSE = 'CLOSE',
}

export const SHOW_TWEEN_DURATION: number = 200;
export const HIDE_TWEEN_DURATION: number = SHOW_TWEEN_DURATION * 0.8;

export default class PopupStandard extends BaseView {
  public scene: SceneBase;
  protected blocker: Phaser.GameObjects.Image | Phaser.GameObjects.Graphics;
  protected blockerZone: Phaser.GameObjects.Zone;
  protected bounds: Phaser.Geom.Rectangle;
  protected blockerAlpha: number;
  protected closeButton: Phaser.GameObjects.Image;

  constructor() {
    const scene: Phaser.Scene = getScene(SceneKey.POPUP);
    super(scene, gameConfig.canvasWidth / 2, gameConfig.canvasHeight / 2);
    this.createBody();
  }

  public preDestroy(): void {
    !!this.blocker && this.blocker.destroy();
    !!this.blockerZone && this.blockerZone.destroy();
    super.preDestroy();
  }

  public show(x: number, y: number, ...args: any[]): void {
    this.x = x;
    this.y = y;
    this.alpha = 0;
    this.visible = true;
    if (this.blocker) {
      this.scene.add.existing(this.blocker);
    }
    this.createShowTween(...args);
  }

  public async hide(actionId?: number): Promise<void> {
    this.scene.tweens.killTweensOf(this);
    await this.createHideTween(actionId);
  }

  public forceHidePopup(): void {
    this.emit(PopupEventKeys.CLOSE, 'close');
  }

  public forceClosePopup(): void {
    this.emit(PopupEventKeys.FORCE_CLOSE);
    this.destroy();
  }

  protected createShowTween(...args: any[]): void {
    if (this.blocker) {
      this.scene.tweens.killTweensOf(this.blocker);
      this.blocker.visible = true;
      this.blocker.alpha = 0;
      this.scene.tweens.add({
        targets: this.blocker,
        alpha: this.blockerAlpha,
        duration: SHOW_TWEEN_DURATION,
        ease: 'Circular.Out',
      });
    }
    const toY: number = this.y;
    this.y += this.bounds.height * 0.5;
    this.alpha = 0;
    this.scene.tweens.add({
      targets: this,
      y: toY,
      alpha: 1,
      duration: SHOW_TWEEN_DURATION,
      ease: 'Circular.Out',
      onStart: () => {
        this.onShowStart();
      },
      onComplete: () => {
        this.onShowComplete(...args);
      },
    });
  }

  protected onShowStart(...args: any[]): void {
    args;
    this.emit(PopupEventKeys.SHOW_START);
  }

  protected onShowComplete(...args: any[]): void {
    args;
    this.emit(PopupEventKeys.SHOW_COMPLETE);
  }

  protected onHideStart(...args: any[]): void {
    args;
    this.emit(PopupEventKeys.HIDE_START);
  }

  protected onHideComplete(...args: any[]): void {
    !!this.blockerZone && this.blockerZone.destroy();
    const actionId: number = args[0];
    this.visible = false;
    this.emit(PopupEventKeys.HIDE_COMPLETE, actionId);
  }

  protected async createHideTween(actionId?: number): Promise<void> {
    return new Promise<void>(
      (resolve: (value?: void | PromiseLike<void>) => void) => {
        if (this.blocker) {
          this.scene.tweens.add({
            targets: this.blocker,
            alpha: 0,
            duration: SHOW_TWEEN_DURATION,
            ease: 'Circular.Out',
            onComplete: () => {
              // this.blocker.visible = false;
              // this.blocker.alpha = this.blockerAlpha;
              // this.scene.sys.displayList.remove(this.blocker);
            },
          });
        }

        const toY: number = this.y + this.bounds.height * 0.5;
        this.scene.tweens.add({
          targets: this,
          y: toY,
          alpha: 0,
          duration: HIDE_TWEEN_DURATION,
          ease: 'Circular.In',
          onStart: () => {
            this.onHideStart();
          },
          onComplete: () => {
            this.onHideComplete(actionId);
            resolve();
          },
        });
      },
    );
  }

  protected createBg(
    key: string,
    frame: string,
    width: number,
    height: number,
  ): NinePatch {
    const config: any = {
      key,
      frame,
      width,
      height,
    };
    const bg: NinePatch = this.scene.make.ninePatch(config, false);
    bg.setInteractive();
    this.add(bg);

    this.bounds = new Phaser.Geom.Rectangle(bg.x, bg.y, bg.width, bg.height);
    return bg;
  }

  protected createBody(): void {
    throw new Error(
      `Method 'createBody' is not implemented in ${this.constructor.name}`,
    );
  }

  protected createCloseButton(
    key: string,
    frame: string,
    x?: number,
    y?: number,
  ): void {
    const config: any = {
      x: x || this.bounds.width * 0.5 - 70,
      y: y || -this.bounds.height * 0.5 + 70,
      key,
      frame,
    };
    this.closeButton = this.scene.make.image(config);
    this.closeButton.setInteractive();
    this.closeButton.on('pointerdown', this.closeClick, this);
    this.add(this.closeButton);
  }

  protected createBlocker(
    _key: string,
    _frame: string,
    alpha: number = 1,
    isInteractive: boolean = false,
  ): void {
    const config: any = {
      x: gameConfig.canvasWidth / 2,
      y: gameConfig.canvasHeight / 2,
      key: _key,
      frame: _frame,
    };
    this.blocker = this.scene.make.image(config);
    this.blocker.setInteractive();
    isInteractive && this.blocker.on('pointerdown', this.blockerClick, this);
    this.blocker.alpha = alpha;
    this.blockerAlpha = this.blocker.alpha;
    this.blocker.visible = false;
    this.blocker.setScale(gameConfig.canvasHeight / this.blocker.height);
  }

  protected createGraphicsBlocker(
    color: number,
    alpha: number = 0.7,
    isInteractive: boolean = false,
  ): void {
    this.blocker = this.scene.make.graphics({});
    this.blocker.fillStyle(color, alpha);
    this.blockerAlpha = this.blocker.alpha;
    this.blocker.visible = false;
    this.blockerZone = this.scene.make.zone({
      x: 0,
      y: 0,
      width: gameConfig.canvasWidth,
      height: gameConfig.canvasHeight,
    });
    this.blockerZone.setOrigin(0);
    this.blockerZone.setInteractive();
    isInteractive &&
      this.blockerZone.once('pointerdown', this.blockerClick, this);
    this.blocker.fillRectShape(this.blockerZone.getBounds());
  }

  private blockerClick(pointer: Phaser.Input.Pointer): void {
    if (this.shape.contains(pointer.x, pointer.y)) {
      return;
    }
    this.closeClick();
  }

  private closeClick(): void {
    // this.scene.events.emit(SceneBase.PLAY_SFX, Audios.PopupClose.Name);
    this.emit(PopupEventKeys.CLOSE, 'close');
  }

  get shape(): Phaser.Geom.Rectangle {
    return new Phaser.Geom.Rectangle(
      this.x - this.bounds.width / 2,
      this.y - this.bounds.height / 2,
      this.bounds.width,
      this.bounds.height,
    );
  }
}
