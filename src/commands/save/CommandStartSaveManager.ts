import { inject, injectable } from '@robotlegsjs/core';
import { ManagerSave } from '../../managers/ManagerSave';
import { CommandBase } from '../base/CommandBase';

@injectable()
export class CommandStartSaveManager extends CommandBase {
  @inject(ManagerSave)
  private managerSave: ManagerSave;

  public execute(): void {
    this.managerSave.start();
    super.execute();
  }
}
