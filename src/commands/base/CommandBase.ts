import { injectable, ICommand } from '@robotlegsjs/core';

@injectable()
export class CommandBase implements ICommand {
  private static readonly consoleArgs: string[] = [
    ``,
    `background: ${'#3F234E'}`,
    `background: ${'#6E2994'}`,
    `color: ${'#D4BFE0'}; background: ${'#8724BD'};`,
    `background: ${'#6E2994'}`,
    `background: ${'#3F234E'}`,
  ];

  public execute(...args: any[]): void {
    CommandBase.consoleArgs[0] = `%c %c %c ${
      this.constructor.name
    }: execute %c %c `;
    console.log.apply(console, CommandBase.consoleArgs);
  }
}
