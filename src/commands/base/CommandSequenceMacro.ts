import { injectable } from '@robotlegsjs/core';
import { SequenceMacro } from '@robotlegsjs/macrobot';

@injectable()
export abstract class CommandSequenceMacro extends SequenceMacro {
  private static readonly consoleArgs: string[] = [
    ``,
    `background: ${'#2A0020'}`,
    `background: ${'#7A005C'}`,
    `color: ${'#FEF2FB'}; background: ${'#9C0075'};`,
    `background: ${'#7A005C'}`,
    `background: ${'#2A0020'}`,
  ];

  public execute(): void {
    CommandSequenceMacro.consoleArgs[0] = `%c %c %c ${
      this.constructor.name
    }: execute %c %c `;
    console.log.apply(console, CommandSequenceMacro.consoleArgs);
    super.execute(arguments);
  }

  protected dispatchComplete(success: boolean): void {
    CommandSequenceMacro.consoleArgs[0] = `%c %c %c ${
      this.constructor.name
    }: execute: complete [${success}] %c %c `;
    console.log.apply(console, CommandSequenceMacro.consoleArgs);
    super.dispatchComplete(success);
  }
}
