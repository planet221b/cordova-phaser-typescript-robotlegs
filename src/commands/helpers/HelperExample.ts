import { inject, injectable } from '@robotlegsjs/core';
import { action } from 'mobx';
import { ModelGame } from '../../models/ModelGame';

@injectable()
export class HelperConstruction {
  @inject(ModelGame)
  private modelGame: ModelGame;

  @action
  public exampleAction(): void {
    // this.modelGame.width = 5
  }
}
