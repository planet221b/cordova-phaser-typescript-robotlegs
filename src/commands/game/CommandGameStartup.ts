import { injectable } from '@robotlegsjs/core';
import { CommandSequenceMacro } from '../base/CommandSequenceMacro';
import { CommandStorageLoad } from '../load/CommandStorageLoad';
import { CommandStartSaveManager } from '../save/CommandStartSaveManager';

@injectable()
export class CommandGameStartup extends CommandSequenceMacro {
  public prepare(): void {
    this.add(CommandStorageLoad);
    this.add(CommandStartSaveManager);
  }
}
