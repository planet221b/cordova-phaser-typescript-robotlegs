import { IInjector, inject, injectable } from '@robotlegsjs/core';
import { deserialize } from 'class-transformer';
import store from 'store';
import { ModelUser } from '../../models/ModelUser';
import { CommandBase } from '../base/CommandBase';

@injectable()
export class CommandStorageLoadUser extends CommandBase {
  @inject(IInjector)
  public injector: IInjector;

  @inject(String)
  public storeKey: string;

  public execute(): void {
    super.execute();
    const theStore: StoreJsAPI = store.namespace(this.storeKey);
    const user: string = theStore.get(ModelUser.STORAGE_KEY, '{}');
    this.injector.bind(ModelUser).toConstantValue(deserialize(ModelUser, user));
  }
}
