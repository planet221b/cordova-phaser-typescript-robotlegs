import { injectable } from '@robotlegsjs/core';
import { SequenceMacro } from '@robotlegsjs/macrobot';
import store from 'store';
import { GAME_STORAGE_KEY } from '../../constants/StoreKeys';
import { CommandStorageLoadGame } from './CommandStorageLoadGame';
import { CommandStorageLoadUser } from './CommandStorageLoadUser';

@injectable()
export class CommandStorageLoad extends SequenceMacro {
  public prepare(): void {
    store.clearAll();
    this.add(CommandStorageLoadUser).withPayloads(GAME_STORAGE_KEY);
    this.add(CommandStorageLoadGame).withPayloads(GAME_STORAGE_KEY);
  }
}
