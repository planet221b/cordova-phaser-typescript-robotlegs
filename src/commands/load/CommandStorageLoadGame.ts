import { IInjector, inject, injectable } from '@robotlegsjs/core';
import { SequenceMacro } from '@robotlegsjs/macrobot';
import { deserialize } from 'class-transformer';
import store from 'store';
import { ModelGame } from '../../models/ModelGame';

@injectable()
export class CommandStorageLoadGame extends SequenceMacro {
  @inject(IInjector)
  public injector: IInjector;

  @inject(String)
  public storeKey: string;

  public prepare(): void {}

  public execute(): void {
    const theStore: StoreJsAPI = store.namespace(this.storeKey);
    const game: string = theStore.get(ModelGame.STORAGE_KEY, `{}`);
    this.injector.bind(ModelGame).toConstantValue(deserialize(ModelGame, game));
    super.execute();
  }
}
