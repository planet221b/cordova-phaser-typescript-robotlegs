import { injectable } from '@robotlegsjs/core';
import { observable } from 'mobx';
import { ModelSerializable } from './ModelSerializable';

@injectable()
export class ModelGame extends ModelSerializable {
  public static readonly STORAGE_KEY: string = 'game';

  @observable
  public rotationDirection: number = 1;
  @observable
  public width: number = 300;
  @observable
  public height: number = 300;

  // @action
  // public changeRotationDirection(): void {
  //   this._rotationDirection = -this._rotationDirection;
  // }

  // @action
  // public resize(width: number, height: number): void {
  //   this._width = width;
  //   this._height = height;
  // }
}
