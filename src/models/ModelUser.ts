import { injectable } from '@robotlegsjs/core';
import { serialize } from 'class-transformer';
import { ModelSerializable } from './ModelSerializable';

@injectable()
export class ModelUser extends ModelSerializable {
  public static readonly STORAGE_KEY: string = 'user';

  public serialize(): string {
    return serialize(this);
  }
}
