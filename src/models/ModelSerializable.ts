import { injectable } from '@robotlegsjs/core';
import { serialize } from 'class-transformer';

@injectable()
export abstract class ModelSerializable {
  public serialize(): any {
    return serialize(this);
  }
}
